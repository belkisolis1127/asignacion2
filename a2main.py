import mysql.connector
from mysql.connector import connect, Error

seguir = True

while seguir == True:
    print("Elige una opcion...")
    print("a) Agregar nueva palabra")
    print("c) Editar palabra existente")
    print("d) Eliminar palabra existente")
    print("e) Ver listado de palabras")
    print("f) Buscar significado de palabra")
    print("g) Salir")

    opcion = input("Elige una opcion (a,c,d,e,f,g) >> ")

    try:

        if opcion.upper() == "A":
            palabra = input("Ingrese la nueva palabra >> ")
            significado = input(
                "Ingrese el significado de la palabra '"+str(palabra)+"' >> ")
            if palabra == "" or significado == "":
                print("Los datos no pueden estar vacios...")
            else:
                try:
                    try:
                        with connect(
                            host="localhost",
                            user="damiandev",
                            password="Damian2019.",
                            db="slang-panameño"
                        ) as connection:
                            with connection.cursor() as cursor:
                                existsPalabra = "SELECT * FROM palabras WHERE lower(palabra) = %(palabraParam)s "
                                cursor.execute(existsPalabra, {
                                               'palabraParam': palabra.lower()})
                                rows = cursor.fetchall()
                                if rows == []:
                                    try:
                                        with connect(
                                            host="localhost",
                                            user="damiandev",
                                            password="Damian2019.",
                                            db="slang-panameño"
                                        ) as connection:
                                            with connection.cursor() as cursor:
                                                cursor.execute("INSERT INTO palabras (palabra, significado) VALUES(%(palabraParam)s,%(significadoParam)s)", {
                                                    'palabraParam': palabra, 'significadoParam': significado})
                                                connection.commit()
                                                print(
                                                    ">> Palabra agregada correctamente")
                                    except Error as e:
                                        print(e)
                                else:
                                    print(">> La palabra '" +
                                          str(palabra)+"' ya existe")
                    except Error as e:
                        print(e)

                except Error as error:
                    print(">> Palabra no agregada correctamente")
                    print(">> Error:", error)
        elif opcion.upper() == "C":
            palabraAEditar = input("Ingrese la palabra a editar >> ").upper()

            if palabraAEditar == "":
                print("Los datos no pueden estar vacios...")
            else:
                try:
                    palabra = input("Ingrese la palabra editada >> ")
                    significado = input(
                        "Ingrese el significado de la palabra editada >> ")
                    with connect(
                        host="localhost",
                        user="damiandev",
                        password="Damian2019.",
                        db="slang-panameño"
                    ) as connection:
                        with connection.cursor() as cursor:
                            existsPalabra = "SELECT * FROM palabras WHERE lower(palabra) = %(palabraAEditarParam)s "
                            cursor.execute(existsPalabra, {
                                'palabraAEditarParam': palabraAEditar.lower()})
                            rows = cursor.fetchall()
                            if rows != []:
                                try:
                                    with connect(
                                        host="localhost",
                                        user="damiandev",
                                        password="Damian2019.",
                                        db="slang-panameño"
                                    ) as connection:
                                        with connection.cursor() as cursor:
                                            cursor.execute("UPDATE palabras SET palabra = %(palabraParam)s, significado = %(significadoParam)s WHERE palabra = %(palabraAEditarParam)s", {
                                                'palabraParam': palabra, 'significadoParam': significado, 'palabraAEditarParam': palabraAEditar})
                                            connection.commit()
                                            print(
                                                ">> Palabra editada correctamente")
                                except Error as e:
                                    print(e)
                            else:
                                print(">> La palabra '" +
                                      str(palabra)+"' no existe")
                except Error as error:
                    print(">> Palabra no editada correctamente")
                    print(">> Error:", error)
        elif opcion.upper() == "D":
            palabraAEliminar = input("Ingrese palabra a eliminar >> ").upper()

            try:
                with connect(
                    host="localhost",
                    user="damiandev",
                    password="Damian2019.",
                    db="slang-panameño"
                ) as connection:
                    with connection.cursor() as cursor:
                        existsPalabra = "SELECT * FROM palabras WHERE lower(palabra) = %(palabraAEliminarParam)s "
                        cursor.execute(existsPalabra, {
                            'palabraAEliminarParam': palabraAEliminar.lower()})
                        rows = cursor.fetchall()
                        if rows != []:
                            try:
                                with connect(
                                    host="localhost",
                                    user="damiandev",
                                    password="Damian2019.",
                                    db="slang-panameño"
                                ) as connection:
                                    with connection.cursor() as cursor:
                                        cursor.execute("DELETE FROM palabras WHERE palabra = %(palabraAEliminarParam)s", {
                                            'palabraAEliminarParam': palabraAEliminar})
                                        connection.commit()
                                        print(
                                            ">> Palabra eliminada correctamente")
                            except Error as e:
                                print(e)
                        else:
                            print(">> La palabra '" +
                                  str(palabra)+"' no existe")
            except Error as error:
                print(">> Palabra no eliminada correctamente")
                print(">> Error:", error)
        elif opcion.upper() == "E":
            with connect(
                host="localhost",
                user="damiandev",
                password="Damian2019.",
                db="slang-panameño"
            ) as connection:
                with connection.cursor() as cursor:
                    palabras = "SELECT * FROM palabras"
                    cursor.execute(palabras)
                    rows = cursor.fetchall()
                    for row in rows:
                        print("-- ID #", row[0], " --")
                        print(">>> PALABRA :", row[1])
                        print(">>> SIGNIFICADO :", row[2])
                        print("")
        elif opcion.upper() == "F":
            palabra = input("Ingrese palabra a buscar >> ").upper()
            with connect(
                host="localhost",
                user="damiandev",
                password="Damian2019.",
                db="slang-panameño"
            ) as connection:
                with connection.cursor() as cursor:
                    sql = "SELECT * FROM palabras WHERE upper(palabra) = %(palabraParam)s"
                    cursor.execute(sql, {'palabraParam': palabra})
                    rows = cursor.fetchall()
                    for row in rows:
                        print("-- ID #", row[0], " --")
                        print(">>> PALABRA :", row[1])
                        print(">>> SIGNIFICADO :", row[2])
                        print("")
        elif opcion.upper() == "G":
            print(">> Saliendo...")
            seguir = False
        else:
            print(">> Opcion no valida...")
            print(">> Cerrando DB...")
            seguir = False
    except Error as error:
        print(">> Error en el proceso: (", error, ")")
        print(">> Cerrando DB...")
        seguir = False
